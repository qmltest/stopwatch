import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello Stopwatch")

    Connections {
        target: stopwatch
        function onValueChanged(str) { display.text = str }
    }

    ColumnLayout {

        // Layout.fillHeight: true
        anchors.fill: parent
        spacing: 6

        Text {
            id: display
            anchors.horizontalCenter: parent.horizontalCenter
            text: "0.00"
        }

        RowLayout {
            Layout.fillWidth: true
            spacing : 10
            Button {
                text: "stop"
                Layout.fillWidth: true
                onClicked: stopwatch.stop()
            }
            Button {
                text: "halt"
                Layout.fillWidth: true
            }
            Button {
                text: "run"
                Layout.fillWidth: true
                onClicked: stopwatch.start()
            }
        }
    }
}
