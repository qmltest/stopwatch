#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QObject>
#include <QTimer>

class Stopwatch : public QObject
{
    Q_OBJECT
public:
    explicit Stopwatch(QObject *parent = nullptr);

signals:
    void valueChanged(QString);
private slots:
    void onTimerExpired(void);

public slots:
    void stop(void);
    void start(void);

private:
    QTimer timer;
    unsigned int time_value{0};

};

#endif // STOPWATCH_H
