#include <QDebug>
#include "stopwatch.h"

Stopwatch::Stopwatch(QObject *parent)
    : QObject{parent}
{

    connect(&timer, &QTimer::timeout, this, &Stopwatch::onTimerExpired);
    timer.setSingleShot(false);
}

void Stopwatch::onTimerExpired(void)
{
    qDebug() << "expired";
    time_value += 1;
    QString str = tr("%1.%2").arg(time_value/10).arg(time_value%10);

    emit valueChanged(str);
}

void Stopwatch::stop()
{
    timer.stop();
}

void Stopwatch::start()
{
    timer.start(100);
}
