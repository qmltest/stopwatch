#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "stopwatch.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    Stopwatch *stopwatch = new Stopwatch(&app);
    engine.rootContext()->setContextProperty("stopwatch", stopwatch);
    const QUrl url(u"qrc:/stopwatch/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
